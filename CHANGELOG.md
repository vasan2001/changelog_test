# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.1.2](https://gitlab.com/vasan2001/changelog_test/compare/v1.1.1...v1.1.2) (2021-06-23)


### Bug Fixes

* changed index.js ([03315ba](https://gitlab.com/vasan2001/changelog_test/commit/03315ba256e38e502141334d086e51f0fdbe8d64))

### [1.1.1](https://gitlab.com/vasan2001/changelog_test/compare/v1.1.0...v1.1.1) (2021-06-22)


### Bug Fixes

* change index.js ([6096eb6](https://gitlab.com/vasan2001/changelog_test/commit/6096eb66217913087da130c2d8344d8d76707b07))

## 1.1.0 (2021-06-22)


### Features

* add standard release support! ([f88f07a](https://gitlab.com/vasan2001/changelog_test/commit/f88f07ad794030afc852365895e0907238a7ec0c))


### Bug Fixes

* create nodejs application ([fde8ec5](https://gitlab.com/vasan2001/changelog_test/commit/fde8ec5a1ad648a2a2f86f4215bf9d39ccb6d993))
